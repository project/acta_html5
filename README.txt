Acta HTML5 is Drupal 7 version of 'Acta' Drupal 5 theme with new code.
While porting same design into Drupal7 I prefere develop it with my own code.
I use HTML5 and css3 code and prefere new project and new name as 'Acta HTML5'.

Acta is a two column fixed design (optimized for width 960px).
I use very few images in theme design.
So it's become more light weight and excute fast then Drupal 5 version.
New design for RSS tag.

Quick step-by-step

Setting up a Drupal site involves many steps.
These are some steps to set up a Drupal site with Acta Theme:

Install Drupal 7.x Install Acta Theme (Put acta-folder in: sites/all/themes).
Enable and make Acta default (admin/appearance)
Edit theme settings (admin/appearance/settings/acta) - Add mission, slogan, etc.
Edit site information (admin/config/system/site-information)
Add Home to Main menu (admin/structure/menu/manage/main-menu/add)

You can view them at http://acta.itapplication.net
